var express=require('express')
var fs = require('fs')

var search=require('./search')
//handles Bing search API and logs each query into a text log
//doesn't yet transfer over the result data into this file as cleanly as it should
        //for instance- pagination shouldnt have to be handled in this server file

var app=express()
var port=process.env.PORT||3000;

app.use('/',express.static('./public'))


app.get('/img/:query*',function(req,res){
  
  var term=req.params.query;
  var page;
   (!isNaN(term))? page=parseInt(term)*10:page=10; 
   //handle pagination 
   
    search.getImg(term,page,function(v){
      var arr=[]
      for (var i=page-1;i>=page-10;i--){//loop in reverse to account for large page numbers
        arr.push(v[i])}
      res.send(arr)
    })//display 10 results in relation to the current page ex.-'/5'=results 51-60
})

app.get('/recent',function(req,res){
  fs.readFile('data.txt', 'utf8', function (err,data) {
    if (err) throw err;
    res.json(data.split(' '))
  });
})//displays the most recent results from the data.txt file

app.listen(port,function(){
  console.log("Express is listening on port "+port)
})
